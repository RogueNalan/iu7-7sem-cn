﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TcpTransferer
{
    public class TcpFileSender : IDisposable
    {
        private readonly Socket fileSocket;

        private readonly IPEndPoint remoteEp;

        private readonly IUIWorker uiWorker;

        private const int maxBufferSize = 32;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public TcpFileSender(IPAddress address, IUIWorker uiWorker)
        {
            log.Trace("TcpFileSender");

            if (address == null)
                throw new ArgumentNullException("address");
            if (uiWorker == null)
                throw new ArgumentNullException("uiWorker");

            fileSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            fileSocket.ReceiveTimeout = ConfigReader.TcpTimeout;
            fileSocket.SendTimeout = ConfigReader.TcpTimeout;

            remoteEp = new IPEndPoint(address, ConfigReader.TcpListenPort);
            this.uiWorker = uiWorker;
        }

        public bool SendFile(string fileName)
        {
            try
            {
                log.Trace("SendFile");

                if (!File.Exists(fileName))
                    return false;

                uiWorker.UpdateStatus("Рассчитываем и отправляем хэш-сумму");
                bool res = ConnectAndSendHash(fileName);
                if (!res)
                    return false;

                uiWorker.UpdateStatus("Отправляем размер файла");
                res = SendSize(fileName);
                if (!res)
                    return false;

                uiWorker.UpdateStatus("Отправляем название файла");
                res = SendFileName(fileName);
                if (!res)
                    return false;

                uiWorker.UpdateStatus("Отправляем файл");
                res = SendFileContents(fileName);
                return res;
            }
            catch (Exception ex)
            {
                log.Info( " ! Ошибка при отправке файла!" + Environment.NewLine + ex );
                return false;
            }
            finally
            {
                uiWorker.UpdateProgress(0);
                uiWorker.UpdateStatus(null);
                fileSocket.Disconnect(true);
            }
        }

        private bool SendSize(string fileName)
        {
            log.Trace("SendSize");

            var fileInfo = new FileInfo(fileName);
            byte[] size = ConfigReader.Encoding.GetBytes(fileInfo.Length.ToString());

            fileSocket.Send(size);
            return GetResponse();
        }

        private bool SendFileName(string fileName)
        {
            log.Trace("SendFileName");

            fileSocket.Send(ConfigReader.Encoding.GetBytes(Path.GetFileName(fileName)));
            return GetResponse();
        }

        private bool ConnectAndSendHash(string fileName)
        {
            log.Trace("ConnectAndSendHash");

            byte[] hash;
            using (var fstream = new FileStream(fileName, FileMode.Open))
                hash = SHA1.Create().ComputeHash(fstream);
            fileSocket.Connect(remoteEp);
            fileSocket.Send(hash);
            return GetResponse();
        }

        private bool SendFileContents(string fileName)
        {
            log.Trace("SendFileContents");
            int maxSize = 1024;
            byte[] fileBuffer = new byte[maxSize];

            using (var nstream = new NetworkStream(fileSocket))
            using (var fstream = new FileStream(fileName, FileMode.Open))
            {
                long fileSize = new FileInfo(fileName).Length;

                int readBytes;
                int sentBytes;

                long sum = 0;
                do
                {
                    readBytes = fstream.Read(fileBuffer, 0, maxSize);
                    sentBytes = fileSocket.Send(fileBuffer, readBytes, SocketFlags.None);
                    sum += sentBytes;
                    
                    int progress = Convert.ToInt32(sum * 100 / fileSize);
                    uiWorker.UpdateProgress(progress);
                    log.Debug($" > Отправка {sentBytes} байт к {remoteEp.Address.ToString()}");
                }
                while (readBytes != 0);
            }
            log.Info($" > Передача файла к {remoteEp.Address.ToString()} завершена");

            return GetResponse();
        }

        public void Dispose()
        {
            log.Trace("TcpFileSender.Dispose");

            fileSocket.Dispose();
        }

        private bool GetResponse()
        {
            log.Debug($" > Ожидаем подтверждения от {remoteEp.Address.ToString()}");

            var buffer = new byte[maxBufferSize];

            fileSocket.Receive(buffer, maxBufferSize, SocketFlags.None);
            string response = ConfigReader.Encoding.GetString(buffer).Replace("\0", String.Empty);
            log.Debug($" > Получен ответ: '{remoteEp.Address.ToString()}'");
            return response == "ok";
        }
    }
}
