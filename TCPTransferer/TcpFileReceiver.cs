﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TcpTransferer
{
    internal class TcpFileReceiver : IDisposable
    {
        private readonly Socket fileSocket;

        public IPAddress RemoteIp { get { return remoteEp?.Address; } }
        private readonly IPEndPoint remoteEp;

        private readonly string remoteIpString;

        public long? FileSize { get { return fileSize; } }
        private readonly long? fileSize = null;

        public string FileName { get { return fileName; } }
        private readonly string fileName;

        private readonly byte[] remoteHash = new byte[hashSize];

        private bool fileTransferFinished = false;

        private const int maxBufferLength = 1024;

        private const int hashSize = 20;

        private byte[] buffer = new byte[maxBufferLength];

        private static readonly byte[] correctResponse = ConfigReader.Encoding.GetBytes("ok");
        private static readonly byte[] wrongResponse = ConfigReader.Encoding.GetBytes("failed");

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly IUIWorker uiWorker;

        public TcpFileReceiver(Socket acceptedSocket, IUIWorker uiWorker)
        {
            log.Trace("TcpFileReceiver");

            if (acceptedSocket == null)
                throw new ArgumentNullException("acceptedSocket");
            if (uiWorker == null)
                throw new ArgumentNullException("uiWorker");

            fileSocket = acceptedSocket;
            fileSocket.ReceiveTimeout = ConfigReader.TcpTimeout;
            fileSocket.SendTimeout = ConfigReader.TcpTimeout;

            this.uiWorker = uiWorker;

            remoteEp = (IPEndPoint)fileSocket.RemoteEndPoint;
            remoteIpString = remoteEp.Address.ToString();

            log.Debug($"Connection from {remoteIpString} accepted");

            uiWorker.UpdateStatus("Получаем хэш-сумму");
            fileSocket.Receive(remoteHash);
            fileSocket.Send(correctResponse);
            log.Debug($"Received hash from {remoteIpString}");

            uiWorker.UpdateStatus("Получаем длину файла");
            fileSocket.Receive(buffer);
            long temp;
            bool res = long.TryParse(ConfigReader.Encoding.GetString(buffer), out temp);
            if (res)
                fileSize = temp;
            fileSocket.Send(correctResponse);
            log.Debug($"Received file size: {temp} from {remoteIpString}");

            uiWorker.UpdateStatus("Получаем название файла");
            fileSocket.Receive(buffer);
            fileName = ConfigReader.Encoding.GetString(buffer).Replace("\0", String.Empty);
            fileSocket.Send(correctResponse);
            log.Debug($"Received file name: {fileName} from {remoteIpString}");
        }

        public bool GetFile(string filename)
        {
            try
            {
                log.Trace("GetFile");

                using (var fstream = new FileStream(filename, FileMode.Create))
                {
                    ReceiveFile(fstream);
                    fstream.Position = 0;
                    var sourceHash = SHA1.Create().ComputeHash(fstream);


                    if (!remoteHash.SequenceEqual(sourceHash))
                    {
                        log.Debug(" > Хэш-суммы не равны, файл принят некорректно");
                        File.Delete(fileName);
                        return false;
                    }
                    else
                    {
                        log.Debug(" > Хэш-суммы равны, файл принят корректно");
                        fileSocket.Send(correctResponse);
                        fileTransferFinished = true;
                        return true;
                    }
                }
            }
            catch (SocketException)
            {
				string msg = "Соединение было разорвано при попытке передачи файла";
                log.Info( " > "+msg );
                uiWorker.Report( msg );
                return false;
            }
            catch (Exception ex)
            {
                log.Error( " ! Ошибка при принятии файла! Процесс передачи завершается." + Environment.NewLine + ex );
                return false;
            }
        }

        private void RefuseLoading()
        {
            log.Trace("RefuseLoading");

            fileSocket.Send(wrongResponse);
            fileSocket.Shutdown(SocketShutdown.Both);
        }

        private void ReceiveFile(Stream recordStream)
        {
            log.Trace("ReceiveFile");

            uiWorker.UpdateStatus("Получаем файл");
            int bytesReceived;
            for (long i = 0; i < FileSize; i += bytesReceived)
            {
                bytesReceived = fileSocket.Receive(buffer);
                recordStream.Write(buffer, 0, bytesReceived);

                log.Debug($" > Принято {bytesReceived} байт от {remoteIpString}. Всего принято: {i} / {FileSize.Value} байт");
                int progress = Convert.ToInt32(i * 100 / FileSize.Value);
                uiWorker.UpdateProgress(progress);
            }
            fileSocket.Send(correctResponse);
            log.Info( $" > Прием файла от {remoteIpString} успешно завершен");
        }

        public void Dispose()
        {
            log.Trace("TcpFileReceiver.Dispose");

            if (!fileTransferFinished)
                RefuseLoading();
            fileSocket.Dispose();
        }
    }
}
