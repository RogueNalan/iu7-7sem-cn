﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTransferer
{
    static class ConfigReader
    {
        public static Encoding Encoding { get { return encoding; } }
        private static Encoding encoding = Encoding.GetEncoding(ConfigurationManager.AppSettings["EncodingName"]);

        public static int TcpListenPort { get { return tcpListenPort; } }
        private static int tcpListenPort = int.Parse(ConfigurationManager.AppSettings["TcpListenPort"]);

        public static int TcpTimeout { get { return tcpTimeout; } }
        private static int tcpTimeout = int.Parse(ConfigurationManager.AppSettings["TcpTimeout"]);
    }
}
