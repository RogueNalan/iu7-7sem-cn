﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpTransferer
{
    public interface IUIWorker
    {
        void Report(string message);
        string GetFileName(string ipAddress, string suggestedFileName);
        void UpdateStatus(string message);
        void UpdateProgress(int value);
    }
}
