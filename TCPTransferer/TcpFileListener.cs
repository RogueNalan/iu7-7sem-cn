﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TcpTransferer
{
    public sealed class TcpFileListener : IDisposable
    {
        private readonly Socket listener;

        private readonly IPEndPoint localEp = new IPEndPoint(IPAddress.Any, ConfigReader.TcpListenPort);

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private const int maxConnections = 1;

        private bool disposed = false;

        private readonly IUIWorker uiWorker;

        public TcpFileListener(IUIWorker uiWorker)
        {
            log.Trace("TcpFileListener");

            if (uiWorker == null)
                throw new ArgumentNullException("uiWorker");
            this.uiWorker = uiWorker;

            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEp);
            listener.Listen(1);
            listener.BeginAccept(new AsyncCallback(ConnectionAccepted), null);
        }

        private void ConnectionAccepted(IAsyncResult result)
        {
            try
            {
                log.Trace("ConnectionAccepted");

                using (var fileRec = new TcpFileReceiver(listener.EndAccept(result), uiWorker))
                {
                    if (fileRec.FileSize.HasValue)
                    {
                        long filesize = fileRec.FileSize.Value;
                        string filename = uiWorker.GetFileName(fileRec.RemoteIp.ToString(), fileRec.FileName);

                        if (!String.IsNullOrWhiteSpace(filename))
                        {
                            if (!CheckFreeSpace(filename, fileRec.FileSize.Value))
                            {
                                var text = $"Недостаточно места на диске для приёма файла размером {filesize} байт";
                                log.Debug(text);
                                uiWorker.Report(text);
                                return;
                            }

                            bool res = fileRec.GetFile(filename);
                            string report = (res) ? "Передача файла успешно завершена" : "При передаче файла возникла ошибка";
                            uiWorker.Report(report);
                        }
                    }
                }
            }
            catch (ObjectDisposedException ex)
            {
                log.Debug(ex);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                uiWorker.UpdateStatus(null);
                uiWorker.UpdateProgress(0);
                if (!disposed)
                    listener.BeginAccept(new AsyncCallback(ConnectionAccepted), null);
            }
        }

        private bool CheckFreeSpace(string filename, long filesize)
        {
            var root = Path.GetPathRoot(filename);
            var info = new DriveInfo(root);
            return info.AvailableFreeSpace >= filesize;
        }

        public void Dispose()
        {
            log.Trace("TcpFileListener.Dispose");

            disposed = true;
            listener.Dispose();
        }
    }
}
