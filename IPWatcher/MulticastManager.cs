﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IPWatcher
{
    public sealed class MulticastManager : IDisposable
    {
        private readonly UdpClient sendingSocket;
        private readonly UdpClient receivingSocket;

        private readonly IPEndPoint localEp = new IPEndPoint(IPAddress.Any, ConfigReader.MulticastPort);
        private readonly IPEndPoint remoteEp = new IPEndPoint(ConfigReader.MulticastAddress, ConfigReader.MulticastPort);

        private readonly object connectionLock = new object();

        private readonly Timer aliveTimer;

        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private static readonly IPAddress localIp = GetNetworkAddresses().First(ip => !ip.Equals(IPAddress.Parse("192.168.56.1"))); // костыль против виртуалбокса

        public bool Connected { get; private set; } = false;


		//////////////////////////////////////////////////////////////////////////

		public MulticastManager()
        {
            log.Trace("MulticastManager");

            sendingSocket = new UdpClient();
            receivingSocket = new UdpClient();
            receivingSocket.Client.Bind(localEp);

            aliveTimer = new Timer(new TimerCallback(TimerTick), null, Timeout.Infinite, ConfigReader.MulticastRefreshTimer);
		}

		public void Dispose()
		{
			log.Trace( "MulticastManager.Dispose()" );

			if ( Connected )
				LeaveMulticast();

			sendingSocket.Dispose();
			receivingSocket.Dispose();
			aliveTimer.Dispose();
		}


		public void JoinMulticast()
        {
            try
            {
                log.Trace("JoinMulticast");

                lock (connectionLock)
                {
                    if (!Connected)
                    {
                        sendingSocket.JoinMulticastGroup(ConfigReader.MulticastAddress, localIp);
                        receivingSocket.JoinMulticastGroup(ConfigReader.MulticastAddress);
                        receivingSocket.BeginReceive(new AsyncCallback(ReceiveMessage), null);
                        aliveTimer.Change(0, ConfigReader.MulticastRefreshTimer);

                        Connected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error( " ! Ошибка при добавлении к списку широковещания!" + Environment.NewLine + ex );
            }
        }

        public void LeaveMulticast()
        {
            try
            {
                log.Trace("LeaveMulticast");

                lock (connectionLock)
                {
                    if (Connected)
                    {
                        string srcMsg = "disconnected";
                        byte[] msg = ConfigReader.Encoding.GetBytes(srcMsg);
                        sendingSocket.Send(msg, msg.Length, remoteEp);
                        log.Debug($"Отправка сообщения при удалении из списка широковещания: '{srcMsg}'");

                        sendingSocket.DropMulticastGroup(ConfigReader.MulticastAddress);
                        receivingSocket.DropMulticastGroup(ConfigReader.MulticastAddress);
                        aliveTimer.Change(Timeout.Infinite, ConfigReader.MulticastRefreshTimer);

                        Connected = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error( " ! Ошибка при удалении из списка широковещания!" + Environment.NewLine + ex );
            }
        }

        private void ReceiveMessage(IAsyncResult result)
        {
            try
            {
                log.Trace("ReceiveMessage");

                IPEndPoint ep = new IPEndPoint(IPAddress.Any, ConfigReader.MulticastPort);
                byte[] msg = receivingSocket.EndReceive(result, ref ep);

                string message = ConfigReader.Encoding.GetString(msg);
                log.Debug($"Получено сообщение '{message}' от {ep.Address.ToString()}");

                lock (connectionLock)
                {
                    if (Connected)
                    {
                        try
                        {
                            if (GetNetworkAddresses().All(addr => !addr.Equals(ep.Address)))
                                RaiseConnection(new AddressState(message, ep.Address));
                        }
                        finally
                        {
                            receivingSocket.BeginReceive(new AsyncCallback(ReceiveMessage), null);
                        }
                    }
                }
            }
            catch (ObjectDisposedException ex)
            {
                log.Debug( " > Ожидаемое исключение при уничтожении сокета." + Environment.NewLine + ex );
            }
            catch (Exception ex)
            {
                log.Error( " ! Ошибка при получении сообщения!" + Environment.NewLine + ex);
            }
        }

        #region ConnectionEvent

        public event EventHandler<AddressState> Connection;

        private void RaiseConnection(AddressState state)
        {
            log.Trace("OnConnection");

            var ev = Connection;
            if (ev != null)
                ev(this, state);
        }

        #endregion

        private void TimerTick(object state)
        {
            try
            {
                log.Trace("TimerTick");
                lock (connectionLock)
                {
                    if (Connected)
                    {
                        string srcMsg = "alive";
                        var msg = ConfigReader.Encoding.GetBytes(srcMsg);
                        sendingSocket.Send(msg, msg.Length, remoteEp);
                        log.Debug($"Отправка сообщения по таймеру: {srcMsg}");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error( " ! Ошибка при отправке сообщения по таймеру!" + Environment.NewLine + ex );
            }
        }

        private static IEnumerable<IPAddress> GetNetworkAddresses()
        {
            log.Trace("GetNetworkAddresses");

            return Dns.GetHostAddresses(Dns.GetHostName())
                .Where(ip => ip.AddressFamily != AddressFamily.InterNetworkV6);
        }
    }
}
