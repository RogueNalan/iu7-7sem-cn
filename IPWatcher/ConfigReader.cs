﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IPWatcher
{
    public static class ConfigReader
    {
        public static IPAddress MulticastAddress { get { return multicastAddress; } }
        private static IPAddress multicastAddress = IPAddress.Parse(ConfigurationManager.AppSettings["MulticastAddress"]);

        public static int MulticastPort { get { return multicastPort; } }
        private static int multicastPort = int.Parse(ConfigurationManager.AppSettings["MulticastPort"]);

        public static int MulticastRefreshTimer { get { return multicastRefreshTimer; } }
        private static int multicastRefreshTimer = int.Parse(ConfigurationManager.AppSettings["MulticastRefreshTimer"]);

        public static Encoding Encoding { get { return encoding; } }
        private static Encoding encoding = Encoding.GetEncoding(ConfigurationManager.AppSettings["EncodingName"]);
    }
}
