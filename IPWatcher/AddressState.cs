﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IPWatcher
{
    public enum State
    {
        Alive,
        Disconnected,
        Unknown
    }

    public class AddressState
    {
        public AddressState()
        {
            CreationTime = DateTime.Now;
        }

        public AddressState(string state, IPAddress address)
        {
            this.Address = address;

            switch(state)
            {
                case "alive": State = State.Alive; break;
                case "disconnected": State = State.Disconnected; break;
                default: State = State.Unknown; break;
            }

            CreationTime = DateTime.Now;
        }

        public State State { get; set; }

        public IPAddress Address { get; set; }

        public DateTime CreationTime { get; set; }

        public override string ToString()
        {
            return Address?.ToString();
        }
    }
}
