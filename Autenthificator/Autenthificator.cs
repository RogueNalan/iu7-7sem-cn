﻿using AutenthificatorService.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

namespace AutenthificatorService
{
    public class Autenthificator : IAutenthificator
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public bool Authenthificate(string login, byte[] passwordHash)
        {
            try
            {
                log.Debug($"Autenthificate: {login}");

                if (login.Length > UserInfo.MaxLoginLength || passwordHash == null || passwordHash.Length != UserInfo.HashSize)
                    return false;

                using (var context = new FileTransfererDb())
                {
                    var data = context.UserDataInfo.AsNoTracking().FirstOrDefault(u => u.Login == login);
                    return (data != null) ?
                        data.PasswordHash.SequenceEqual(passwordHash)
                        : false;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
                return false;
            }
        }

        public static void HostAutenthificator()
        {
            try
            {
                log.Trace("HostAutenthificator");

                Uri autenthificatorAddress = new Uri(ConfigReader.Url + ConfigReader.Service);

                var host = new ServiceHost(typeof(Autenthificator), autenthificatorAddress);

                var binding = new WSHttpBinding();
                binding.Security.Mode = SecurityMode.None;
                host.AddServiceEndpoint(typeof(IAutenthificator), binding, ConfigReader.Url + ConfigReader.IAutenthificator);

                var beh = new ServiceMetadataBehavior();
                beh.HttpGetEnabled = true;
                host.Description.Behaviors.Add(beh);

                host.Open();

                log.Info("Служба аутентификации запущена");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
    }
}
