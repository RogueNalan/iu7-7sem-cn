﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AutenthificatorService
{
    [ServiceContract]
    public interface IAutenthificator
    {
        [OperationContract]
        bool Authenthificate(string login, byte[] password);
    }
}
