﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutenthificatorService
{
    class ConfigReader
    {
        public static string Url { get; } = ConfigurationManager.AppSettings["Url"];

        public static string Service { get; } = ConfigurationManager.AppSettings["Service"];

        public static string IAutenthificator { get; } = ConfigurationManager.AppSettings["IAutenthificator"];
    }
}
