﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutenthificatorService.Data
{
    public class UserInfo
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(maxLoginLength)]
        [Index(IsUnique=true)]
        public string Login { get; set; }

        [Required]
        [MaxLength(hashSize)]
        public byte[] PasswordHash { get; set; }

        [NotMapped]
        public static int MaxLoginLength { get { return maxLoginLength; } }
        private const int maxLoginLength = 32;

        [NotMapped]
        public static int HashSize { get { return hashSize; } }
        private const int hashSize = 20;
    }
}
