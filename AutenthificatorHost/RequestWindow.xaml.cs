﻿using AutenthificatorHost.ViewModels;
using AutenthificatorService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutenthificatorHost
{
    /// <summary>
    /// Interaction logic for RequestWindow.xaml
    /// </summary>
    public partial class RequestWindow : Window
    {
        public RequestWindow()
        {
            InitializeComponent();
        }

        internal RequestWindow(string login)
            : this()
        {
            DataContext = new RequestWindowViewModel(login);
            addNew = String.IsNullOrEmpty(login);
        }

        private readonly bool addNew;

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = (RequestWindowViewModel)DataContext;
            if (String.IsNullOrEmpty(dc.Login))
            {
                MessageBox.Show("Логин не может быть пустым!");
                return;
            }
            if (addNew && String.IsNullOrEmpty(dc.Password))
            {
                MessageBox.Show("Пароль не может быть пустым!");
                return;
            }
            DialogResult = true;
            Close();
        }
    }
}
