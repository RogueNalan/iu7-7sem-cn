﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AutenthificatorHost
{
    /// <summary>
    /// Usual WPF pattern. Provides common-used command instead of specific class for each required command.
    /// </summary>
    sealed class DelegateCommand : ICommand
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public DelegateCommand(Action<object> execute)
            : this(execute, null)
        {

        }

        public DelegateCommand(Action<object> execute, Predicate<object> canExecute)
        {
            log.Trace("DelegateCommand");

            if (execute == null)
                throw new ArgumentNullException();

            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            log.Trace("CanExecute");
            return (canExecute != null) ? canExecute(parameter) : true;
        }

        public void Execute(object parameter)
        {
            log.Trace("Execute");
            execute(parameter);
        }

        public event EventHandler CanExecuteChanged;
        public void RaiseCanExecuteChanged()
        {
            log.Trace("RaiseCanExecuteChanged");
            EventHandler temp = CanExecuteChanged;
            if (temp != null)
            {
                temp(this, EventArgs.Empty);
            }
        }

        private readonly Action<object> execute;

        private readonly Predicate<object> canExecute;
    }
}
