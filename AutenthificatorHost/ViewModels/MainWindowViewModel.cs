﻿using AutenthificatorService;
using AutenthificatorService.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace AutenthificatorHost.ViewModels
{
    sealed class MainWindowViewModel : INotifyPropertyChanged, IDisposable
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly FileTransfererDb context;

        public ICollectionView UserInfo { get; }

        public object SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                bool check = (selectedItem != null && value == null) || (selectedItem == null && value != null);
                selectedItem = value;
                RaisePropertyChanged("SelectedItem");
                if (check)
                    ProcessSelectedItemChange();
            }
        }
        private object selectedItem;

        private bool CanExecuteOnItem { get { return SelectedItem != null; } }

        public DelegateCommand RefreshCommand { get; }

        public DelegateCommand AddCommand { get; }

        public DelegateCommand EditCommand { get; }

        public DelegateCommand DeleteCommand { get; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public MainWindowViewModel()
        {
            log.Trace("MainWindowViewModel");

            Autenthificator.HostAutenthificator();

            context = new FileTransfererDb();
            context.UserDataInfo.Load();

            UserInfo = new CollectionView(context.UserDataInfo.Local); // Потому что ObservableCollection тупая и не всегда умеет в рефреш + нельзя запустить кастомно

            RefreshCommand = new DelegateCommand(o => Refresh());
            AddCommand = new DelegateCommand(o => Add());

            EditCommand = new DelegateCommand(o => Edit(), o => CanExecuteOnItem);
            DeleteCommand = new DelegateCommand(o => Delete(), o => CanExecuteOnItem);
        }

        public void Refresh()
        {
            try
            {
                log.Debug("Обновляем данные");
                for (int i = 0; i < context.UserDataInfo.Local.Count; ++i)
                    context.Entry(context.UserDataInfo.Local[i]).State = EntityState.Detached;
                context.UserDataInfo.Load();
                UserInfo.Refresh();
            }
            catch (Exception e)
            {
                log.Error(e);
                MessageBox.Show("Произошла ошибка при обновлении данных!");
            }
        }

        public void Add()
        {
            try
            {
                log.Trace("Add");
                var newData = MakeRequest("");

                if (newData?.PasswordHash != null)
                {
                    log.Debug($"Добавляем нового пользователя {newData.Login}");
                    context.UserDataInfo.Add(newData);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                log.Error(e);
                MessageBox.Show("Произошла ошибка при добавлении пользователя!");

                Refresh();
            }
        }

        public void Edit()
        {
            try
            {
                log.Trace("Edit");
                var info = (UserInfo)SelectedItem;
                var newData = MakeRequest(info.Login);

                if (newData != null)
                {
                    log.Debug($"Редактируем пользователя {info.Login}");
                    info.Login = newData.Login;
                    info.PasswordHash = newData.PasswordHash ?? info.PasswordHash;
                    UserInfo.Refresh();
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                log.Error(e);
                MessageBox.Show("Произошла ошибка при редактировании пользователя!");

                Refresh();
            }
        }

        public void Delete()
        {
            try
            {
                var info = (UserInfo)SelectedItem;
                log.Debug($"Удаляем пользователя {info.Login}");
                context.UserDataInfo.Remove(info);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                log.Error(e);
                MessageBox.Show("Произошла ошибка при удалении пользователя!");

                Refresh();
            }
        }

        public void Dispose()
        {
            log.Trace("MainWindowViewModel.Dispose");
            context.Dispose();
        }

        private void ProcessSelectedItemChange()
        {
            log.Trace("ProcessSelectedItemChange");
            EditCommand.RaiseCanExecuteChanged();
            DeleteCommand.RaiseCanExecuteChanged();
        }

        private UserInfo MakeRequest(string login)
        {
            log.Trace("MakeRequest");
            var request = new RequestWindow(login);
            var data = (RequestWindowViewModel)request.DataContext;
            bool? res = request.ShowDialog();
            return (res.HasValue && res.Value) ? data.GetUserInfo() : null;
        }
    }
}
