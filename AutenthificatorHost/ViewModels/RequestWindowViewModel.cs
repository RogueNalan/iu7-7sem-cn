﻿using AutenthificatorService.Data;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AutenthificatorHost.ViewModels
{
    sealed class RequestWindowViewModel : INotifyPropertyChanged
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        public RequestWindowViewModel()
        {
            log.Trace("RequestWindowViewModel");
        }

        public RequestWindowViewModel(string login)
        {
            log.Trace("RequestWindowViewModel");
            Login = login;
        }

        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
                RaisePropertyChanged("Login");
            }
        }
        private string login;

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
                RaisePropertyChanged("Password");
            }
        }
        private string password;

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            log.Trace("RaisePropertyChanged");
            PropertyChangedEventHandler temp = PropertyChanged;
            if (temp != null)
            {
                temp(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public UserInfo GetUserInfo()
        {
            log.Trace("GetUserInfo");
            var hash = (password != null) ? SHA1.Create().ComputeHash(Encoding.Default.GetBytes(Password))
                : null;
            return new UserInfo
            {
                Login = this.Login,
                PasswordHash = hash
            };
        }
    }
}
